package com.twuc.webApp;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class PathController {
    @GetMapping("/api/users/{userId}")
    String getSuccess(@PathVariable Integer userId) {
        return "success" + userId;
    }

    @GetMapping("/api/books/{id}")
    String bindToInt(@PathVariable int id) {
        return "int success" + id;
    }

    @GetMapping("/api/users/{userId}/books/{bookId}")
    String userBook(@PathVariable int userId, @PathVariable int bookId ) {
        return String.format("user %d book %d", userId, bookId);
    }

    @PostMapping("/api/people")
    String getPerson(@RequestBody @Valid Person person) {
        return person.getName();
    }

    @PostMapping("/api/datetimes")
    String getDataTime(@RequestBody DateTest date) {
        return date.getDateTime().toString();
    }

    @PostMapping("/api/students")
    String getStudent(@RequestBody @Valid Student student) {
        return student.getName();
    }
}
