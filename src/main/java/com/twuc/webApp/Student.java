package com.twuc.webApp;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class Student {
    private String name;
    @Min(1000)
    @Max(9999)
    private int yearOfBirth;
    @Email
    private String email;

    public Student(String name, int yearOfBirth, String email) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.email = email;
    }

    public Student() {
    }

    public String getName() {
        return name;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public String getEmail() {
        return email;
    }
}
