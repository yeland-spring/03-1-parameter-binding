package com.twuc.webApp;

import java.time.ZonedDateTime;

public class DateTest {
    private ZonedDateTime dateTime;

    public DateTest(ZonedDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public DateTest() {
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }
}
