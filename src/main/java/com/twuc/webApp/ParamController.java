package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
class ParamController {
    @GetMapping("/api/users")
    String getName(@RequestParam String name) {
        return  "My name is " + name;
    }

    @GetMapping("/api/books")
    String getBookName(@RequestParam(defaultValue = "Math") String name) {
        return  "This is " + name;
    }

    @GetMapping("/api/authors")
    String getIdList(@RequestParam List<String> id) {
        return "Ids are " + id;
    }

}