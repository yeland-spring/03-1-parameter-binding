package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class ParamControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_message_when_use_requestParam() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users?name=Mary"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("My name is Mary"));
    }

    @Test
    void should_return_message_given_no_parameter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_message_when_have_default_parameter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/books"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("This is Math"));
    }

    @Test
    void should_return_message_when_parameter_is_collection() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/authors?id=1,2,3"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("Ids are [1, 2, 3]"));
    }
}