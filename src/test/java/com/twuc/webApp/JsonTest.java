package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class JsonTest {

    private ObjectMapper objectMapper;

    @Test
    void should_convert_object_to_json() throws JsonProcessingException {
        Person person = new Person("Mary", "female");
        objectMapper = new ObjectMapper();
        String result = objectMapper.writeValueAsString(person);
        assertEquals("{\"name\":\"Mary\",\"gender\":\"female\"}", result);
    }

    @Test
    void should_convert_json_to_object() throws IOException {
        String string = "{\"name\":\"Mary\",\"gender\":\"female\"}";
        ObjectMapper objectMapper = new ObjectMapper();
        Person person = objectMapper.readValue(string, Person.class);
        assertEquals("Mary", person.getName());
    }
}