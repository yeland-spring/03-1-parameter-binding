package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class PathControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_success_when_bind_to_Integer() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("success2"));
    }

    @Test
    void should_return_success_when_bind_to_int() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/books/2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("int success2"));
    }

    @Test
    void should_return_message_when_have_two_variable() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/2/books/322"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("user 2 book 322"));
    }

    @Test
    void should_return_message_when_post_request_body() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/people").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"Mary\",\"gender\":\"female\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("Mary"));
    }

    @Test
    void should_return_message_when_post_dataTime() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"dateTime\":\"2019-10-01T10:00:00Z\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("2019-10-01T10:00Z[UTC]"));
    }

    @Test
    void should_return_message_when_dataTime_not() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"dateTime\":\"2019-10-01\"}"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_200_when_person_name_not_null() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/people").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new Person("Mary", "female"))))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("Mary"));
    }

    @Test
    void should_return_400_when_person_name_is_null() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/people").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":,\"gender\":\"female\"}"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_400_when_string_out_size() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/people").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new Person("abcdefgh", "female"))))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_name_when_in_boundary() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/students")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new Student("Lily", 1996, "lily@163.com"))))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("Lily"));
    }

    @Test
    void should_return_400_when_exceed_boundary() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/students")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new Student("Lily", 996, "lily@163.com"))))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_400_when_email_illegal() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/students")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new Student("Lily", 1996, "lily"))))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }
}
